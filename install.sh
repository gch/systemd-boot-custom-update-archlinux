#!/bin/bash

set -e
set -x

if [ ! -f gen/cmdline.tpl ]; then
	awk '
			BEGIN {
				ORS = RS = " "
			}

			!/^root=/ && !/^initrd=/ && !/^systemd\.machine_id=/
		' \
		/proc/cmdline \
	| awk '{ $1 = $1 }; 1' >gen/cmdline.tpl
fi

rootfs=`
	awk '
			BEGIN {
				ORS = RS = " "
			} /^root=/
		' \
		/proc/cmdline \
	| awk '{ $1 = $1 }; 1'`

echo -n "$rootfs " >gen/cmdline
cat gen/cmdline.tpl >>gen/cmdline

install -o root -m 0755 scripts/custom-update-systemd-boot /usr/local/bin/
install -o root -m 0644 scripts/99-custom-systemd-boot.hook /etc/pacman.d/hooks/
install -o root -m 0755 scripts/70-custom-initrd.install /etc/kernel/install.d/
install -o root -m 0755 scripts/80-custom-initrd-acpi-override.install /etc/kernel/install.d/
install -o root -m 0644 gen/cmdline /etc/kernel/
